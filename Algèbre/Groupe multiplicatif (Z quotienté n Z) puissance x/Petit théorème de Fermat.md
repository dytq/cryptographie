#théorème 
# Théorème
Soit $n \geq 2$ un entier naturel. Soit $a\in \mathbb{Z}$ tel que le $pgcd(a,n)=1$ [[PGCD]] alors:
$$a^{\phi(n)}=1 \text{ mod } n$$

[[Fonction indicatrice d'Euler]]
