#définition 
# Définition
La fonction indicatrice d'Euler est une fonction $\phi:\mathbb{N}\rightarrow\mathbb{N}$ définie par:
$$n\mapsto\phi(n)=| \{ k \text{ tel que } 1\leq k\leq n \text{ et } 
pgcd(k,n=1) \} |$$
[[PGCD]]
