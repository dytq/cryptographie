#théorème
# Théorème
Soient $n$ et $m$ deux [[Entiers premiers entre eux]]. Alors pourt tout $a$, $b$, Il existe un unique entier $x$ modulo $nm$ tel que 
$$x=a \text{ mod } n \text{ et } x=b \text{ mod } m$$
