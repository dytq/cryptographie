# Groupe
**tags**: #définition 

Soit $G$ un ensemble muni d'une [[Loi de Composition]] $\circ$. On dit que $(G,\circ)$, c'est à dire que $G$ muni de la loi $\circ$, est un groupe si:
- G possède un [[Élément Neutre]] $e$ relativement à la loi $\circ$.
- La loi $\circ$ est [[Associativité]].
- Tout élément de $G$ est [[Inversible ou Symétrisable]] par rapport à la loi $\circ$

# Exemple

# Propriétés

![[Groupe commutative ou abélien]]

![[Sous-groupe]]

# Groupes finis

![[Ordre d'un Groupe]]

![[Théorème de Lagrange]]

# Groupe Symétrique

![[Groupe Symétrique]]