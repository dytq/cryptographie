**tags**: #définition 

Soit $(G,\circ)$ un [[Groupe]], si de plus la loi $\circ$ est [[Commutativité]], alors $(G,\circ)$ est une groupe commutatif ou abélien. 

