**tags:** #définition 

L'odre d'un [[Groupe]] ($G,\circ$) est le cardinal de $G$.