**tags:** #définition 

Soit $(G,\circ)$ un [[Groupe]] et soit $H \subset G$ . On dit que $H$ est un sous-groupe de   $(G,\circ)$ si:
- $H$ est [[Loi Induite - Partie Stable]] pour la loi $\circ$, ie $\forall(x,y)\in H^2,x\circ y \in H$.
- muni de la loi induite, $H$ est un [[Groupe]].