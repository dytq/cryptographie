**tags**: #définition 

Pour tout entier $n \geq 1$, on note $E_{n}= \{1,\dotsc, n\}$. On appelle [[Groupe]] symétrique d'indice $n$ le [[Groupe]] noté  $\mathcal{S}_n$ de toutes les permutations de $E_n$. 