**tags:** #théorème

Soit $(G,\circ)$ un [[Groupe]] fini et $H$ un [[Sous-groupe]] de $G$. Alors l'ordre de $H$ divise l'ordre de $G$.