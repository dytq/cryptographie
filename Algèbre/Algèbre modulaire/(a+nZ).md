**tags:** #définition 

Soit $a$ et $n$ deux entiers, l'ensemble $a+n\mathbb{Z}$ [[(nZ)]] est défini par
$$a+n\mathbb{Z}:=\{k\in \mathbb{Z}, \exists j \in n\mathbb{Z}, k = a + jn\}$$
