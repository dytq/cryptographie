**tags:** #définition 

Soit $n$ un entier naturel. Deux entiers relatifs $a$ et $b$ sont dits congrus modulo $n$ si leur différence est divisible par $n$ [[Divisiblité]], c'est à dire si $a$ est de la forme $b+kn$ avec $k$ entier.
La congruence est une [[Relation d'Équivalence]] pour tout $n$ non nul.