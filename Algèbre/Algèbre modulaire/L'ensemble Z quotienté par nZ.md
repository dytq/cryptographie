**tags:** #définition 

Soit $n$ un entier non nul. On définit l'ensemble $\mathbb{Z}$ quotienté par $n\mathbb{Z}$ [[(nZ)]] noté $\mathbb{Z}/ n\mathbb{Z}$ par l'expression:
$$\mathbb{Z}/n\mathbb{Z}:=\{a+n\mathbb{Z}\},a\in\mathbb{Z}$$
