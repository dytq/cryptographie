**tags:** #définition 

Soit $\circ$ une loi sur un ensemble $E$. On dit que la loi $\circ$ est commutative si $\forall (x,y) \in E \times E, x\circ y = y \circ x$. 