**tags:** #définition 

Soit $\circ$ une loi sur un ensemble $E$. On dit que la loi $\circ$ est associative si $\forall x,y,z \in E, (x\circ y) \circ z = x \circ (y \circ z)$. 