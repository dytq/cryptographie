**tags:** #définition 

Soit $E$ un ensemble muni d'une loi de composition $\circ$.  Soit $e$ un élément de $E$. On dit que $e$ est un élément neutre pour la loi $\circ$ si $\forall x \in E, a \circ e = e \circ a = a$

## Proposition
L'élément neutre de $E$  pour la loi $\circ$, s'il existe est unique.