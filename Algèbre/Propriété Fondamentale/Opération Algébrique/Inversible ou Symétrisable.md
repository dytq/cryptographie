**tags:** #définition 

Soit $E$ un ensemble muni de la loi $\circ$ qui possède un élément neutre de $e$ (relativement à la loi $\circ$). Soit $x$ un élément de $E$. On dit que $x$ est inversible (ou symétrisable) pour la loi $\circ$ s'il existe un élément $x'$  de $E$ tel que $x\circ x' = x'\circ x =e$. Si un tel élément existe, il est unique et on l'appelle l'inverse de x.