**tags:** #définition 

Soit $E$ un ensemble muni de deux lois $\circ$ et $\cdot$ . On dit que la loi $\circ$ est ditributive par rapport à la loi $\cdot$ si pour tout $x$, $y$, $z$ dans $E$

$$x\circ (y \cdot z) = (x \circ y) \cdot (x \circ z)$$
et
$$(x\cdot y) \circ z = (x \circ z) \cdot (y \circ z) $$



