#définition 
# Définition
Une loi de composition sur un ensemble $E$ quelconque est une [[Fonction]] de $E\times E$ vers $E$. 