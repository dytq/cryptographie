#définition 
# Définition
Soit $E$ un ensemble muni de la loi $\circ$, et $F$ un sous-ensemble de $E$. On dit que $F$ est stable pour la loi $\circ$ si $\forall (x,y) \in F \times F, x \circ y \in F$. La restriction à $F \times F$ de la loi $\circ$ définit alors une loi de composition sur $F$ appelée loi induite, généralement notée de la même manière.