#théorème 
# Théorème de Bézout
Soient $a$ et $b$ non tous nuls, alors il existe $u$ et $v$ dans $\mathbb{Z}$ tels que 
$$au+bv = pgcd(a,b)$$[[PGCD]]
