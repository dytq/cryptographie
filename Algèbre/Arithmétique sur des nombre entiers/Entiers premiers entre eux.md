#définition 
# Définition
On dit que deux entiers $a$ et $b$ sont premiers entre eux si leur [[PGCD]] = 1, $pgcd(a,b)=1$.