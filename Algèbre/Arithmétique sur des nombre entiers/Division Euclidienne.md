#définition 
# Définition
Soit $(a,b) \in \mathbb{Z}\times\mathbb{N}^{*}$. Alors il exite un unique couple $(q,r)$ de $\mathbb{Z}\times\mathbb{N}$ tel que $a = bq+r$ et $0\leq r \leq b -1$. Le couple $(q,r)$ est le résultat de la division euclidienne de $a$ par $b$. Dans cette division, $a$ est le dividende, $b$ le diviseur [[Divisiblité]], $q$ le quotient et $r$ le reste.