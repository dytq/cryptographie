#définition 
# Définition
Soient $a$ et $b$ deux entiers relatifs. Il existe un unique entier naturel $n$ tel que $a\mathbb{Z} + b\mathbb{Z}=n\mathbb{Z}$ [[(nZ)]]. On dit que $n$ est le pgcd de $a$ et de $b$. On notera $n = pgcd(a,b)$.