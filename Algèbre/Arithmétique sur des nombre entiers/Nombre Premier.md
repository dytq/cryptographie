#définition 
# Définition
On dit que $p \in \mathbb{Z}$ est un nombre premier si $p\geq 2$ et si ses seuls diviseurs sont $1$ et $p$. [[Divisiblité]]