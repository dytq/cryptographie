#théorème 
# Définition
Tout entier $n\geq 2$ s'écrit $n=p^{\alpha_1}_{1} p^{\alpha_2}_{2} \dotsc p^{\alpha_m}_m$ où
- $m$ est un entier strictement positif.
- Les $p_i$ sont des [[Nombre Premier]] deux à deux distincts.
- Les $\alpha_i$ sont des entiers strictement positifs.
Un telle écriture est unique à l'ordre des facteurs près.