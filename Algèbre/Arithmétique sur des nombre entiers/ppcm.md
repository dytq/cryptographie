#définition 
# Définition
Soient $a$, $b$ dans $\mathbb{Z}$. il existe un unique $n$ dans $\mathbb{N}$ tel que $a\mathbb{Z}\cap b\mathbb{Z} = n\mathbb{Z}$ [[(nZ)]]. On dit que $n$ est le ppcm de $a$ et $b$. 