#définition 
# Définition
Soient $a$ et $b$ deux entiers relatifs. On dit que $b$ est un diviseur de $a$  ou encore que $a$ est un multiple de $b$ et on note $b|a$ s'il existe un entier relatif $q$ tel que $a=qb$.