**tags:** #définition 

Soit $A$ un anneau non réduit à $\{0\}$. Soit $a$ un élément non nul de $A$. On dit que $a$ est un diviseur de zéro s'il existe $b$ non nul dans $A$ tel que $ab = 0$ ou $ba=0$.