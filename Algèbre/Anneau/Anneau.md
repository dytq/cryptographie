**tags:** #définition  

# Anneau

Soit $A$ un ensemble muni de deux [[Loi de Composition]] noté $+$ et $\times$. On dit que $(A,+,\times)$ est un anneau si:
- $(A,+)$ est un [[Groupe commutative ou abélien]] (son [[Élément Neutre]] est généralement noté $0$).
- La loi $\times$est [[Associativité]] et elle est [[Distributivité]] par rapport à l'addition +.
- Il existe un [[Élément Neutre]] pour le produit $\times$, en général noté 1.

# Exemple


![[Anneau commutatif ou abélien]]

![[Sous Anneau]]

# Élément Remarquables dans un Anneau 

![[Diviseur de zéro]]

![[Anneau Intègre]]

![[Élément Nilpotents]]

![[Corps]]