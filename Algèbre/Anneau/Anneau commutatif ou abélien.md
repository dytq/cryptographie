**tags:** #définition

Soit un [[Anneau]] $(A,+,\times)$ est commutatif, si de plus la loi $\times$ est commutative.