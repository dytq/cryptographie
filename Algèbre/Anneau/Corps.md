**tags:** #définition 

Soit $K$ un ensemble muni de deux [[Loi de Composition]] + et $\times$. On dit que $(K,+,\times)$ est un corps si:
- $(K,+,\times)$ est un [[Anneau commutatif ou abélien]] non réduit à $\{0\}$.
- $K^{\times}=K^{*}=K \backslash \{0\}$, c'est à dire que tout élément non nul de $K$ est [[Inversible ou Symétrisable]] pour le produit. 