**tags:** #définition 

Soit $A$ un [[Anneau]] non réduit à $\{0\}$. Soit $a$ un élément non nul de $A$. On dit que $a$ est nilpotent s'il existe un entier naturel $n$ tel que $a^{n}= 0$. Avec ses notations, $\forall p \geq n, a^{p}=0$. Le plus peit entier $n$ tel que $a^{n}=0$est appelé indice de nilpotence de $a$.