**tags:** #définition 

On dit qu'un [[Anneau]] $(A,+,\times)$ est intègre s'il est [[Commutativité]] et sans [[Diviseur de zéro]]. Un anneau intègre est donc un [[Anneau commutatif ou abélien]] dans lequel $ab=0 \Rightarrow a=0$ ou $b=0$. 